import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtreTalent'
})
export class FiltreTalentPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!value)return null;
    if(!args)return value;
    args=args.toLowerCase();
    
    return value.filter(function(item){
      //JSON.stringify(item.competences)
      let fullname = (item.nom +' '+item.prenom).toLowerCase();
      return fullname.includes(args);
    })
  }

}
