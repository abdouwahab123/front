import { Component, OnInit } from '@angular/core';
import { CvService } from '../providers/cv.service';

@Component({
  selector: 'app-entreprise',
  templateUrl: './entreprise.component.html',
  styleUrls: ['./entreprise.component.scss']
})
export class EntrepriseComponent implements OnInit {

  constructor(private cvService: CvService) { }
  cvs: any = []
  img: any = []
  cv: any = []
  search = '';
  // cv: any = {};
  ngOnInit() {
    this.cvService.liste().subscribe(
      (data: any) => {
        this.cvs = data.body;
        console.log('cvs', this.cvs)
      }
    )
    this.cvService.liste1().subscribe(
      (data: any) => {
        this.img = data.body;
        console.log('img', this.img)
      }
    )
  }

  Afficher(id) {
    this.cvService.get(id).subscribe(
      (data: any) => {
        this.cv = data.body;
        console.log('cv', this.cv)
      },
      (error: any) => {
        alert('Erreur lors de la récupération du pizza ');
        console.log(error);
      }
    );
  }

}
