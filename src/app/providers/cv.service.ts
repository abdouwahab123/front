import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CvService {

  constructor(private http: HttpClient) { }

  liste() {
    return this.http.get(environment.BASE_URL + '/talent');
  }
  liste1() {
    return this.http.get(environment.BASE_URL + '/audio');
  }

  // add(data){
  //   return this.http.post(environment.BASE_URL+'/cv',data);
  // }
  
  add(data) {
    return this.http.post(environment.BASE_URL + '/talent', data);
  }

  get(id) {
    return this.http.get(environment.BASE_URL + '/talent/' + id);
  }

  edit(id, body) {
    return this.http.put(environment.BASE_URL + '/talent/' + id, body);
  }

  delete(id) {
    return this.http.delete(environment.BASE_URL + '/talent/' + id);
  }
  deletecompetence(id) {
    return this.http.delete(environment.BASE_URL + '/competence/' + id);
  }
  deletelangue(id) {
    return this.http.delete(environment.BASE_URL + '/langue/' + id);
  }
  
  login(body) {
    return this.http.post(environment.BASE_URL + '/login', body);
  }
  addcompetence(data) {
    return this.http.post(environment.BASE_URL + '/competence', data);
  }
  addmail(data) {
    return this.http.post(environment.BASE_URL + '/mail', data);
  }
  editcompetence(id, data) {
    return this.http.put(environment.BASE_URL + '/competence/' + id, data);
  }

  addformation(data) {
    return this.http.post(environment.BASE_URL + '/formation', data);
  }
  addexperience(data) {
    return this.http.post(environment.BASE_URL + '/experience', data);
  }
  addlangue(data) {
    return this.http.post(environment.BASE_URL + '/langue', data);
  }

}
