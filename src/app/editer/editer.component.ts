import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CvService } from '../providers/cv.service';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';


@Component({
  selector: 'app-editer',
  templateUrl: './editer.component.html',
  styleUrls: ['./editer.component.scss']
})
export class EditerComponent implements OnInit {
  // CvTalent: any;
  cv: any;
  competence: FormGroup;
  formation: FormGroup;
  experience: FormGroup;
  langue: FormGroup;
  talent: any ;


  constructor(private cvService: CvService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.cv = JSON.parse(localStorage.getItem('user'));


  }

  initForm() {
    this.competence = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      niveau: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      type: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.cv._id)
    });

    this.formation = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lieu: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      date: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.cv._id)
    });

    this.langue = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      niveau: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.cv._id)
    });

    this.experience = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      debut: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      fin: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      entreprise: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      mission: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      description: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.cv._id)
      // talent: new FormControl('')
    });
  }

  create(competence) {
    if (!competence.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addcompetence(competence.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }
  create1(formation) {
    if (!formation.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addformation(formation.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }
  create2(experience) {
    if (!experience.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addexperience(experience.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }
  create3(langue) {
    if (!langue.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addlangue(langue.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }


  telechargerPdf(talent) {
    // const nom = talent;
    const data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      const pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save(this.cv.nom + 'MYPdf.pdf'); // Generated PDF
    });
  }

}
