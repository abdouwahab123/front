import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CvService } from '../providers/cv.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-liste-cv',
  templateUrl: './liste-cv.component.html',
  styleUrls: ['./liste-cv.component.scss']
})
export class ListeCvComponent implements OnInit {

  constructor(private route: ActivatedRoute, private cvService: CvService) { }
  cvs: any = [];
  search = '';
  cv: any = [];
  img: any = []
  // cv: any = {};
  ngOnInit() {
    this.cvService.liste().subscribe(
      (data: any) => {
        this.cvs = data.body;
        console.log('cvs', this.cvs);
      }
    );
    this.cvService.liste1().subscribe(
      (data: any) => {
        this.img = data.body;
        console.log('img', this.img)
      }
    )
  }
  // routerLink="/pizza/{{pizza._id}}/show"
  // get(id) {
  //   return this.http.get(this.env.BASE_URL + '/pizza/' + id);
  // }
  // delete(id) {
  //   this.pizzaService.delete(id).subscribe(
  //       (success) => {
  //         this.pizzas.map((pizza, index) => {
  //           if (pizza._id == id) {
  //             this.pizzas.splice(index,1);
  //           }
  //         });
  //       }, error => {
  //         alert('une erreur s\'est produite lors de la suppression');
  //         console.log(error);
  //       }
  //   );
  // }

  Afficher(id) {
    this.cvService.get(id).subscribe(
      (data: any) => {
        this.cv = data.body;
        console.log('cv', this.cv)
      },
      (error: any) => {
        alert('Erreur lors de la récupération du pizza ');
        console.log(error);
      }
    );
  }
  update(id) {
    this.cvService.edit(id, { toPublish: true }).subscribe(
      (rep: any) => {
        alert('Publié avec succés')
      },
      error => {
        alert('erreur lors de la publication')
        console.log('error lors de l\'ajout ', error)
      }
    )
  }
  delete(id) {
    this.cvService.edit(id, { toPublish: false }).subscribe(
      (rep: any) => {
        alert('SUCCESSFULL !')
      },
      error => {
        alert('ERROR')
        console.log('error', error)
      }
    )
  }

}
