import { Component, OnInit } from '@angular/core';
import { CvService } from '../providers/cv.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FiltreTalentPipe } from '../filtre-talent.pipe';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  // talent: any;
  competence: FormGroup;
  formation: FormGroup;
  experience: FormGroup;
  langue: FormGroup;
  talent: any = [] ;
  comp: any [] = this.talent.competences ;
  lang: any [] = this.talent.langues ;

  constructor(private cvService: CvService) { }

  ngOnInit() {
    this.talent = JSON.parse(localStorage.getItem('user'));

    this.initForm();

  }
  delete(id) {
    this.cvService.deletecompetence(id).subscribe(
        (success) => {
          this.comp.map((talent, index) => {
            if (talent._id == id) {
              this.comp.splice(index, 1);
            }
          });
        }, error => {
          alert('une erreur s\'est produite lors de la suppression');
          console.log(error);
        }
    );
  }
  delete1(id) {
    this.cvService.deletelangue(id).subscribe(
        (success) => {
          this.lang.map((talent, index) => {
            if (talent._id == id) {
              this.lang.splice(index, 1);
            }
          });
        }, error => {
          alert('une erreur s\'est produite lors de la suppression');
          console.log(error);
        }
    );
  }

  initForm() {
    this.competence = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      niveau: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      type: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.talent._id)
    });

    this.formation = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lieu: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      date: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.talent._id)
    });

    this.langue = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      niveau: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.talent._id)
    });

    this.experience = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      debut: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      fin: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      entreprise: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      mission: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      description: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      talent: new FormControl(this.talent._id)
      // talent: new FormControl('')
    });
  }

  create(competence) {
    if (!competence.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addcompetence(competence.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }

  create1(formation) {
    if (!formation.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addformation(formation.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }
  create2(experience) {
    if (!experience.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addexperience(experience.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }
  create3(langue) {
    if (!langue.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addlangue(langue.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }

}
// delete(id) {
//   this.pizzaService.delete(id).subscribe(
//       (success) => {
//         this.pizzas.map((pizza, index) => {
//           if (pizza._id == id) {
//             this.pizzas.splice(index,1);
//           }
//         });
//       }, error => {
//         alert('une erreur s\'est produite lors de la suppression');
//         console.log(error);
//       }
//   );
// }