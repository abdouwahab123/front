import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EditerComponent } from './editer/editer.component';
import { PageAdminComponent } from './page-admin/page-admin.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AccueilComponent } from './accueil/accueil.component';
import { ProfilComponent } from './profil/profil.component';
import { FiltreTalentPipe } from './filtre-talent.pipe';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { SendMailComponent } from './send-mail/send-mail.component';
import { ListeCvComponent } from './liste-cv/liste-cv.component';

@NgModule({
  declarations: [
    AppComponent,
    EditerComponent,
    PageAdminComponent,
    ListeCvComponent,
    LoginComponent,
    AccueilComponent,
    ProfilComponent,
    FiltreTalentPipe,
    EntrepriseComponent,
    SendMailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
