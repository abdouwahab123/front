import { Component, OnInit } from '@angular/core';
import { CvService } from '../providers/cv.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-send-mail',
  templateUrl: './send-mail.component.html',
  styleUrls: ['./send-mail.component.scss']
})
export class SendMailComponent implements OnInit {
  competence: FormGroup;
  talent: any ;
  constructor(private cvService: CvService) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.competence = new FormGroup({
      contactName: new FormControl('', Validators.compose([
        Validators.required
      ])),
      contactEmail: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      contactObject: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      contactMessage: new FormControl('')
    });
  }
  create(competence) {
    if (!competence.valid) {
      alert('Remplissez tous les champs');
    } else {
      this.cvService.addmail(competence.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();
          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error)
          }
      );
    }
  }
}
