import { Component, OnInit } from '@angular/core';
import { CvService } from '../providers/cv.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private cvService: CvService, private router: Router) { }
  form: FormGroup;
  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.form = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });
  }
  login(form) {
    if (!form.valid) {
      alert('Remplissez tous les champs ');
    } else {
      this.cvService.login(form.value).subscribe(
          (rep: any) => {
            localStorage.setItem('user', JSON.stringify(rep.body));
            if ((rep.body.email == "carine@gmail.com") && (rep.body.password == "carine@")) {
              this.router.navigate(['/liste']);
            } else {this.router.navigate(['/accueil']);
               this.initForm();
          }

          },
          error => {
            alert('erreur lors de l\'enregistrement');
            console.log('error lors de l\'ajout ', error);
          }
      );
    }
  }

}
