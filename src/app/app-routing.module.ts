import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditerComponent } from './editer/editer.component';
import { PageAdminComponent} from './page-admin/page-admin.component';
import { ListeCvComponent} from './liste-cv/liste-cv.component';
import { LoginComponent} from './login/login.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ProfilComponent } from './profil/profil.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { SendMailComponent } from './send-mail/send-mail.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'admin', component: PageAdminComponent},
  {path: 'liste', component: ListeCvComponent},
  {path: 'editer', component: EditerComponent},
  {path: 'accueil', component: AccueilComponent},
  {path: 'profil', component: ProfilComponent},
  {path: 'entreprise', component: EntrepriseComponent},
  {path: 'mail', component: SendMailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
