import { Component, OnInit } from '@angular/core';
import {CvService} from "../providers/cv.service";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  cv:any = [];
  constructor(private cvService: CvService) { }

  ngOnInit() {
    this.cvService.liste().subscribe(
      (data: any)=>{
        this.cv = data.body;
      }
  )
  }

}
